package com.cejv416.polymorphismexample.inheritance;

/**
 * This is a simple class that will be the superclass for this demonstration of
 * polymorphism
 *
 * @author omniprof
 */
public class InheritanceSuperClass {

    /**
     * This method is not overridden in the subclass
     */
    public void doInheritanceMethod() {
        System.out.println("SuperClass - doInheritanceMethod");
    }

    /**
     * This method is overridden in the subclass. It is not required to be
     * overridden.
     */
    public void doInheritOverride() {
        System.out.println("SuperClass - doInheritOverride");
    }
}
